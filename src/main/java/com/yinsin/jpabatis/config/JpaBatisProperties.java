package com.yinsin.jpabatis.config;

import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.data.jpa.repository.JpaRepository;

@ConfigurationProperties(prefix = "batis")
@ConditionalOnClass(JpaRepository.class)
public class JpaBatisProperties {
	
	private String mapper;

	public String getMapper() {
		return mapper;
	}

	public void setMapper(String mapper) {
		this.mapper = mapper;
	}
}
