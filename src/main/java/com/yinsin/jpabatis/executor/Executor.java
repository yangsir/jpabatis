/**
 *    Copyright 2009-2015 the original author or authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package com.yinsin.jpabatis.executor;

import java.sql.SQLException;
import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.data.domain.Page;

import com.yinsin.jpabatis.cache.CacheKey;
import com.yinsin.jpabatis.mapper.BoundSql;
import com.yinsin.jpabatis.mapper.MappedStatement;
import com.yinsin.jpabatis.session.ResultHandler;
import com.yinsin.jpabatis.session.RowBounds;

/**
 * @author Clinton Begin
 */
public interface Executor {

	ResultHandler<?> NO_RESULT_HANDLER = null;
	
	<E> E query(MappedStatement ms, Object parameter, ResultHandler<?> resultHandler, CacheKey cacheKey, BoundSql boundSql) throws SQLException;

	<E> E query(MappedStatement ms, Object parameter, ResultHandler<?> resultHandler) throws SQLException;

	<E> List<E> queryList(MappedStatement ms, Object parameter, ResultHandler<?> resultHandler, CacheKey cacheKey, BoundSql boundSql) throws SQLException;
	
	<E> List<E> queryList(MappedStatement ms, Object parameter, ResultHandler<?> resultHandler) throws SQLException;
	
	<E> Page<E> queryList(MappedStatement ms, Object parameter, RowBounds rowBounds, ResultHandler<?> resultHandler, CacheKey cacheKey, BoundSql boundSql) throws SQLException;
	
	<E> Page<E> queryList(MappedStatement ms, Object parameter, RowBounds rowBounds, ResultHandler<?> resultHandler) throws SQLException;

	<E> E doQuery(MappedStatement ms, Object parameter, ResultHandler<?> resultHandler, BoundSql boundSql) throws SQLException;
	
	<E> List<E> doQueryList(MappedStatement ms, Object parameter, ResultHandler<?> resultHandler, BoundSql boundSql) throws SQLException;
	
	<E> Page<E> doQueryList(MappedStatement ms, Object parameter, RowBounds rowBounds, ResultHandler<?> resultHandler, BoundSql boundSql) throws SQLException;
	
	CacheKey createCacheKey(MappedStatement ms, Object parameterObject, RowBounds rowBounds, BoundSql boundSql);

	boolean isCached(MappedStatement ms, CacheKey key);

	void clearLocalCache();

	EntityManager getTransaction();

	boolean isClose();

	void setExecutorWrapper(Executor executor);
}
