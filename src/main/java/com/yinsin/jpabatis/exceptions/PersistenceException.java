package com.yinsin.jpabatis.exceptions;

public class PersistenceException extends JpaBatisException {
	
	private static final long serialVersionUID = 1702407001556554593L;

	public PersistenceException() {
		super();
	}

	public PersistenceException(String message) {
		super(message);
	}

	public PersistenceException(String message, Throwable cause) {
		super(message, cause);
	}

	public PersistenceException(Throwable cause) {
		super(cause);
	}
}
